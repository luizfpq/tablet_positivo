# Tutorial para os tablets Positivo #

O interesse deste tutorial **NÃO** é de criar meios de acesso ilícitos
aos tablets Positivo YPY\_AB10EC com a firmware bloqueada pela FNDE, mas
sim facilitar o acesso às pessoas que realmente necessitam e vem tendo
dificuldades recorrentes com o sistema de cadastro do CPF, esse passo-a-passo não descaracteriza o equipamento no hardware ou software,
sendo que toda a parte gráfica desenvolvida/implementada pelo MEC e pela
FNDE é mantida.

## Estrutura deste repositório ##

* [Apps](Apps) - Contém os softwares necessários para execução do procedimento
* [LaTex](LaTex) - Contém os arquivos com o [tutorial em PDF](https://bitbucket.org/luizfpq/tablet_positivo/raw/master/LaTex/README.pdf), README.MD, Screenshots e fontes do Tex.

## Observações ##

Seja responsável, use todo o conhecimento que adquirir neste tutorial para facilitar a vida das pessoas e melhorar o ambiente de trabalho.  Conhecimento requer responsabilidade!
