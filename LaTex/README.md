Considerações iniciais
======================

O interesse deste tutorial **NÃO** é de criar meios de acesso ilícitos
aos tablets Positivo YPY\_AB10EC com a firmware bloqueada pela FNDE, mas
sim facilitar o acesso às pessoas que realmente necessitam e vem tendo
dificuldades recorrentes com o sistema de cadastro do CPF, esse passo-a-passo não descaracteriza o equipamento no hardware ou software,
sendo que toda a parte gráfica desenvolvida/implementada pelo MEC e pela
FNDE é mantida.

Softwares necessários
=====================

#### Clicando nos links abaixo, baixe os softwares indicados e deixe-os em um cartão de memória (*SD Card*), para facilitar o procedimento

-   [Kingo
    Root](https://bitbucket.org/luizfpq/tablet_positivo/raw/master/aplicativos/KingoRoot.apk)

-   [ES File
    Explorer](https://bitbucket.org/luizfpq/tablet_positivo/raw/master/aplicativos/es-file-explorer.apk)

Passo-a-passo do procedimento
=============================

1.  Inicialize o sistema seguindo o processo padrão, lembre-se de ativar
    o Wi-Fi, é muito importante que tenhamos conexão com a internet para
    que o procedimento funcione corretamente.

2.  Durante o processo de inicialização não é necessário adicionar as
    contas do Google ou da Positivo.

3.  Avance todas as etapas deixando o equipamento nas configurações
    padrão até que a tela da FNDE seja aberta, como vemos na
    figura [telaFnde].

    ![Tela de cadastro do
    FNDE](Screenshots/Screenshot_1999-12-31-23-34-27.png)
    [telaFnde]

4.  [item:googleUnlock] Ao chegar na tela exemplificada pela
    figura [telaFnde] ou na opção de inserir o CPF, bloqueie o aparelho
    usando o botão Power, após a tela apagar, pressione novamente o
    botão de modo a ativar novamente a tela, mas ao tocar na tela de
    desbloqueio selecione a opção Google ao invés do cadeado que
    normalmente liberaria a tela, como vemos na
    figura [fig:googleUnlock].

    ![Desbloqueio de tela com busca no
    Google](Screenshots/Screenshot_1999-12-31-23-34-46.png)
    [fig:googleUnlock]

5.  Lembre-se que a barra de navegação não estará habilitada, portanto,
    sempre que precisar voltar durante uma operação, terá que refazer o
    passo demonstrado no item [item:googleUnlock].

6.  [item:buscaGoogle] Na barra de busca digite ES File Explorer,
    durante a digitação ele irá mostrar os aplicativos já instalados,
    vide figura [gerenciadorArquivos].

    ![Gerenciador de
    arquivos](Screenshots/Screenshot_2000-01-01-00-07-51.png)
    [gerenciadorArquivos]

7.  [item:es-file-explorer] Ao abrir o Es File Explorer, teremos que
    abrir a pasta ‘sdcard’ e acessar a subpasta ‘external\_sd’, dentro
    desta pasta iremos executar os aplicativos baixados.

    -   Para executar os aplicativos basta abri-los, como abriria uma
        foto por exemplo, o Android saberá que se trata de um executável
        e o tratará como um programa a instalar.

8.  Execute o arquivo ‘KingoRoot.apk’, após clicar no aplicativo o
    Android irá nos notificar que a instalação de aplicativos de fontes
    desconhecidas está bloqueada como visto na
    figura [instalacaoBloqueada], selecione o botão Configurações.

    ![Configurações de
    Instalação](Screenshots/Screenshot_2000-01-01-00-08-18.png)
    [instalacaoBloqueada]

9.  Após selecionar o botão Configurações mostrado na
    figura [instalacaoBloqueada], teremos acesso a tela de configurações
    do sistema, visível na figura [fontesDesconhecidas], no menu lateral
    esquerdo selecione a opção ‘Segurança’ e na lista de configurações
    do lado direito ative a opção ‘Fontes Desconhecidas’.

    ![Ativação de Fontes
    desconhecidas](Screenshots/Screenshot_2000-01-01-00-08-51.png)
    [fontesDesconhecidas]

10. Para executar o ROOT também precisamos liberar a depuração USB, para
    tanto, selecione no menu lateral esquerdo selecione o item ‘Opções
    do desenvolvedor’ e na lista de configurações do lado direito ative
    a opção ‘Depuração USB’, como vemos na figura [depuracaoUsb].

    ![Ativação de depuração
    USB](Screenshots/Screenshot_2000-01-01-00-09-11.png)
    [depuracaoUsb]

11. Após a liberação da depuração USB e da instalação de aplicativos de
    fonte desconhecida, voltamos aos itens [item:googleUnlock]
    e [item:buscaGoogle]. Desta vez podemos executar o ‘KingoRoot.apk’
    sem que haja mensagem de bloqueio.

12. [item:kingoroot] O Kingo Root é bastante amigável, ao abri-lo
    veremos um grande botão azul na parte inferior da tela, com o texto
    ‘One Click Root’, para executar o ROOT basta clicar neste botão e
    aguardar alguns instantes até que o processo finalize. A
    figura [processoRoot] ilustra as telas que o Kingo Root mostrará ao
    usuário durante o processo.

    -   Há um tutorial com todos os procedimentos relativos ao kingo
        root disponível em
        <https://www.kingoapp.com/root-tutorials/how-to-root-android-without-computer.htm>
        (em inglês).

    -   A figura [processoRoot], ilustra as três etapas principais do
        kingo root, sendo elas, da esquerda para a direita: Tela
        inicial, processo de root em andamento, resultado do processo de
        root.

    ![Processo de
    Root](Screenshots/Screenshot_2000-01-01-00-10-34.png)
    [processoRoot]

13. Terminado processo de ROOT, iremos bloquear novamente o
    tablet(item [item:googleUnlock]), abrir o
    Google(item [item:buscaGoogle]) e pesquisar pelo aplicativo
    ‘Removedor de App de Sistema’. Nossa busca retornará o resultado
    ilustrado na figura [instalarRemovedor], clique no botão ‘Instalar’
    (para isso teremos de configurar uma conta Google na Play Store).

    ![Instalar Removedor de App de Sistema
    ](Screenshots/Screenshot_2017-08-02-16-28-47.png)
    [instalarRemovedor]

14. [item:removerAppSistema] Após a instalação do ‘Removedor de App de
    Sistema’ temos que abrir o aplicativo e acessar o menu(canto
    superior direito da tela) para habilitar as funções de exibição de
    nome do pacote e caminho do arquivo APK, como ilustrado na
    figura [removerAppSistema] .

    -   Quando o fizermos seremos notificados da necessidade de aceitar
        o acesso ao ROOT, pelo Kingo Root, deve-se aceitar.

    ![Configuração do removedor de aplicativos de
    sistema](Screenshots/Screenshot_2017-08-02-16-45-15.png)
    [removerAppSistema]

15. Após executar o item [item:removerAppSistema] procure na lista de
    aplicativos por quaisquer referências aos termos ‘mec’ e ‘fnde’ e os
    remova.

    -   Para remover os aplicativos selecione-os clicando um a um, na
        lista mostrada pelo ‘Removedor de App de Sistema’ e após
        selecionar todos os desejados clique no botão vermelho com o
        texto ‘Desinstalar’, visto na parte inferior da
        figura [removerAppSistema1].

    -   Lembre-se de olhar com atenção, pois alguns aplicativos tem o
        título diferente do nome real da aplicação, como por exemplo o
        ‘system.core’ selecionado na figura [removerAppSistema1], que na
        verdade é o **mec**.android.powerservice*.

    ![Remoção de
    aplicativos](Screenshots/Screenshot_2017-08-02-16-45-35.png)
    [removerAppSistema1]

16. Logo após remover os aplicativos, realizaremos novamente os passos
    indicados entre os itens [item:googleUnlock]
    a [item:es-file-explorer], no entanto, ao abrirmos a pasta
    ‘external\_sd’ executaremos o arquivo ‘es-file-explorer.apk’, ao
    executar este instalador o ‘Es File Explorer’ será atualizado.

17. Com o ‘Es File Explorer’ atualizado, teremos um novo menu lateral
    que aparecerá automaticamente dentro do aplicativo, neste novo menu
    lateral temos que ativar as opções ‘Mostrar Ficheiros Ocultos’ e
    ‘Explorador Root’, como pode ser visualizado na
    figura [fig:remocaoArquivos].

    -   Ao ativar essas opções teremos que permitir a mensagem de root
        assim como fizemos anteriormente no removedor de
        aplicativos(item [item:removerAppSistema]).

    ![Remoção de
    Arquivos](Screenshots/Screenshot_2017-08-02-16-47-13.png)
    [fig:remocaoArquivos]

18. Depois de autorizar as permissões, vamos usar a ferramenta
    ‘Procurar’ (Lupa), na barra inferior do Es File Explorer, buscaremos
    então pelo termo ‘MEC’ (Veja figura [fig:buscaArquivos]) e iremos
    apagar todos os arquivos que aparecerem como resultado da busca.

    ![Resultado da busca por
    arquivos](Screenshots/Screenshot_2017-08-02-16-47-26.png)
    [fig:buscaArquivos]

19. Neste momento, nosso dispositivo já está completamente liberado como
    vemos na figura [fig:resultado].

    ![Resultado](Screenshots/Screenshot_2017-08-02-16-56-10.png)
    [fig:resultado]

20. Após a execução de todas as etapas do procedimento é importante que
    seja removido o aplicativo Kingo Root, pois o mesmo é responsável
    por propagandas e mensagens que podem incomodar o usuário.

21. Após a remoção do Kingo Root deve-se realizar um Hard Reset.

Como realizar um Hard Reset
===========================

1.  Para realizar o hard reset é importante que a bateria esteja com
    carga acima de 30%, preferencialmente com a carga completa;

2.  Desligue o aparelho;

3.  Aperte juntos os botões de Power e Volume UP até o LED azul piscar,
    neste momento pode liberá-los;

4.  Aguarde aparecer a imagem do Android com um ícone de exclamação;

5.  Nesta tela mantenha pressionado o botão Power e dê um clique rápido
    no botão volume UP, o aparelho entrará em modo recovery, solte todos
    os botões imediatamente;

6.  Dentro do modo recovery utilize os botões de Volume UP e Volume Down
    para navegar até a opção ‘wipe data/factory reset’, confirme a opção
    com o botão power;

7.  No submenu do factory reset navegue novamente até a opção ‘Yes –
    Delete all user data’, confirme com o botão Power;

8.  Navegue até a opção ‘wipe cache partition’, confirme a opção com o
    botão power;

9.  Após o procedimento, use a opção ‘reboot system now’ com o botão
    Power e aguarde o aparelho ligar normalmente, a partir disso o
    aparelho estará habilitado para uso.
